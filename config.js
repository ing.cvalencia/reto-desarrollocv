const apiKey = "af59e8c9";

const Config = {
    /******* Parametros  de API */
    urlAPI: "https://www.omdbapi.com/?apikey=" + apiKey + "&t=",
    /******* Parametros  MONGO DB */
    db: "mongodb://localhost:27017/MovieNode",
    port: 8080
};

module.exports = Config;