/*****INPORTARCION DE PKG  ***********/

//keys
const key = require("./config.js");

//server
const Koa = require('koa');

//
const bodyParser = require('koa-bodyparser');

//rutas
const router = require('./src/routes');

//Esquema base datos
const mongoose = require('./src/db/conec.js');

//Inicio Server
const app = new Koa();

//// Middelwares ////
app.use(bodyParser());
app.use(router.routes());

app.use(async(req) => {
    req.body = '404 Not Found'
})

app.listen(key.port, () => console.log('Server started on port ' + key.port));