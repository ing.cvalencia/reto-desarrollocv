//keys
const key = require("../../config.js");
const mongoose = require('mongoose');

/***  Conexion a Mongo DB con interface mongoose****/

mongoose.connect(key.db, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:')); // enlaza el track de error a la consola (proceso actual)
db.once('open', () => {
    console.log('connected a Mongo DB'); // si esta todo ok, imprime esto
});

module.exports = mongoose;