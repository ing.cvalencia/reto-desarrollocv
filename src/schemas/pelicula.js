const Joi = require('joi')

const peliculaSchema = Joi.object({
    Title: Joi.string().min(3).max(255),
    Year: Joi.number(),
})

const peliculaUpdateSchema = Joi.object({
    Title: Joi.string().min(3).max(255),
    find: Joi.string().min(3).max(255),
    replace: Joi.string().min(3).max(255),
})

const peliculaHeaderSchema = Joi.object({
    pageNumber: Joi.number(),
    movieYear: Joi.number(),
})

module.exports = { peliculaSchema, peliculaUpdateSchema, peliculaHeaderSchema }