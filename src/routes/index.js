const Router = require('koa-router');
const router = new Router();

const peliculaRouter = require('./peliculas');

router.use('/pelicula', peliculaRouter.routes());

module.exports = router;