const axios = require('axios');
const Router = require('koa-router')
const router = new Router()
const key = require("../../config.js");
const Pelicula = require('../models/pelicula')

const {
    peliculaSchema,
    peliculaUpdateSchema,
    peliculaHeaderSchema,
} = require('../schemas/pelicula')

//Configuracion de Paginacion.
const pageSize = 5;

router.get('/', async(req) => {
    req.body = "Bienvenido Microservicio de Peliculas";
});


/*1) END POINT Buscador de películas (GET):
Reglas de Negocio
- El valor a buscar debe ir en la URL de la API => Listo
- Adicionalmente puede ir un header opcional que contenga el año de la película ==> Listo.
- Almacenar en una BD Mongo, la siguiente info: Title, Year, Released, Genre, Director, Actors, Plot, Ratings == Listo
- El registro de la película solo debe estar una vez en la BD ==> Listo
- Devolver la información almacenada en la BD ==> Listo.
*/
router.get('/busqueda/:title', async(req) => {


    const pageNumber = req.request.headers['x-pelicula-pagina'] || 1;
    const movieYear = req.request.headers['x-pelicula-year'];
    const busqueda = req.params.title;

    //Validacion entrada con JOI
    const vldPAge = peliculaHeaderSchema.validate({ pageNumber: pageNumber });


    if (vldPAge.error)
        req.throw(400, vldPAge.error.ValidationError);

    const vldYear = peliculaHeaderSchema.validate({ movieYear: movieYear || 2022 });
    if (vldYear.error)
        req.throw(400, vldYear.error.ValidationError);

    const vldTitle = peliculaSchema.validate({ Title: busqueda });
    if (vldTitle.error)
        req.throw(400, vldTitle.error.ValidationError);


    //Una vez validado los paramentros de entrada pasamos a obtener informacion de la API omdbapi

    const resp = await axios.get(key.urlAPI + busqueda + "&y=" + movieYear, {
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Access-Control-Allow-Origin': '*'
        },
        data: {} // envío un cuerpo (aunque sea un objeto vacío)
    });

    const pelicula = resp.data;


    //luego hacemos un bulk en nuestra base de datos la respuesta


    if (pelicula.Response = 'True') {
        var r = pelicula.Ratings;

        const p1 = new Pelicula();

        p1.title = pelicula.Title;
        p1.year = pelicula.Year;
        p1.released = pelicula.Released;
        p1.genre = pelicula.Genre;
        p1.director = pelicula.Director;
        p1.actors = pelicula.Actors;
        p1.plot = pelicula.Plot;

        if (r) {
            r.forEach(e => {
                p1.ratings.push(e);
            })
        }
        console.log(pelicula)
        await p1.save(function(err, p) {
            if (err) return console.error(err);
            console.log("pelicula guardada");
        });

    } else {

        req.throw(404, 'Pelicula no fue econtrada');
    }

    //devolvermos lo del la base datos
    const fil = {}


    if (busqueda)
        fil.title = { $regex: busqueda, $options: 'i' };
    if (movieYear)
        fil.year = movieYear;
    const result = await Pelicula.find(fil)
        .skip((pageNumber - 1) * pageSize)
        .limit(pageSize);

    if (!result)
        req.throw(404, 'Pelicula no fue econtrada');

    req.set('x-pelicula-page', pageNumber);
    req.body = result;
});


/*2) END POINT Obtener todas las películas: (GET): 
- Se deben devolver todas las películas que se han guardado en la BD => listo.
- Si hay más de 5 películas guardadas en BD, se deben paginar los resultados de 5 en 5  => listo
- El número de página debe ir por header.  => listo */
router.get('/lista', async(req) => {
    const pageNumber = req.request.headers['x-pelicula-pagina'] || 1;
    const result = await Pelicula.find()
        .skip((pageNumber - 1) * pageSize)
        .limit(pageSize)

    if (!result)
        req.throw(404, 'No existen registros de Peliculas')

    req.set('x-peliculas-page', pageNumber)
    req.body = result;
});


/*3) END POINT  Buscar y reemplazar (post)
- Que reciba en el BODY un object como por ej: 
{
  title: star wars, 
  find: jedi, 
  replace: CLM Dev 
}
- Buscar dentro de la BD y obtener el campo PLOT del registro ==> Listo
- Al string del plot obtenido buscar la palabra enviada en el Body (find) y reemplazar todas sus ocurrencias por el campo enviado en el body (replace) ==> Listo
- Devolver el string con las modificaciones del punto anterior ==> Listo */

router.post('/replacePlot', async(req) => {
    //validacion de JOI
    const { values, error } = peliculaUpdateSchema.validate(req.request.body)

    if (error)
        req.throw(400, error.ValidationError)


    //asignamos parametros de entrada
    const title = req.request.body.Title;
    const find = req.request.body.find.toString();
    const replace = req.request.body.replace;

    //buscamos pelicula por el titulo
    const result = await Pelicula.find({ title: { $regex: title } });;

    if (!result) {
        req.throw(404, 'Pelicula no Econtrada');
    }

    if (req.request.body) {
        //hacemos replace

        var escapedFind = find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");

        result[0].plot = result[0].plot.replace(new RegExp(escapedFind, 'g'), replace);

        const updateR = result[0].save();

        req.body = result[0].plot;
    }


});

module.exports = router;