const { Schema, model } = require('mongoose')

const peliculaSchema = new Schema({
    title: { type: String, default: "" },
    year: { type: String, default: "" },
    released: { type: Date, default: Date.now },
    genre: { type: String, default: "" },
    director: { type: String, default: "" },
    actors: { type: String },
    plot: { type: String, default: "" },
    ratings: [{ Source: String, Value: String }],
})

peliculaSchema.index({ title: 1, year: 1 });

module.exports = model('Peliculas', peliculaSchema)